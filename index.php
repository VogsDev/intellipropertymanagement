<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Intellipropertmanagement &mdash; Website Template by Si-Tshaba</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Modise -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,900|Oswald:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <link rel="stylesheet" href="css/jquery.fancybox.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/style.css">
    
      
  </head>
  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
  
  <div> 

    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>
   
    
    <header class="site-navbar py-4 js-sticky-header site-navbar-target" role="banner">

      <div class="container">
        <div class="row align-items-center">
          
          <div class="col-6 col-xl-2">
            <h1 class="mb-0 site-logo m-0 p-0"><a href="index.html#home-section" class="mb-0"><img src="images/intelli%20property%20Man%20Logo.png" alt="Image" class="img-fluid"></h1>
          </div>

                   <div class="col-12 col-md-10 d-none d-xl-block">
            <nav class="site-navigation position-relative text-right" role="navigation">

              <ul class="site-menu main-menu js-clone-nav mr-auto d-none d-lg-block">
                <li><a href="#home-section" style="color:#FFD700"    class="nav-link">Home</a></li>
                <li><a href="#MessageFromTheCEO-section"  class="nav-link">Message From The CEO</a></li>
                <li><a href="#rentals-section"  class="nav-link">Rentals</a></li>
                <!-- <li><a href="#ourteam-section"  class="nav-link">Our Team</a></li> --->
                <li><a href="#services-section" class="nav-link">Services</a></li>
                <li><a href="#successstories-section" class="nav-link">Success Stories</a></li>
                <li><a href="#contact-section" class="nav-link">Contact</a></li>
                <li><a href="#reportafault-section" class="nav-link">Report a fault</a></li>   
              </ul>
            </nav>
          </div>


          <div class="col-6 d-inline-block d-xl-none ml-md-0 py-3"><a href="#" class="site-menu-toggle js-menu-toggle text-white float-right"><span class="icon-menu h3"></span></a></div>

        </div>
      </div>
      
    </header>

    
    <div class="site-block-wrap">
    <div class="owl-carousel with-dots">
      <div class="site-blocks-cover overlay overlay-2" style="background-image: url(images/hero_1.jpg);" data-aos="fade" id="home-section">


        <div class="container">
          <div class="row align-items-center justify-content-center">
            <div class="col-md-6 mt-lg-5 text-center">
              <h1 class="text-shadow">Let Us Find You A Home ...</h1>
              <p class="mb-5 text-shadow">Making our cities feels like home!</p>
              <p><a href="https://ecashmeup.com/rentalapplication?guid=768c586c2b9e069c96f33dae9eb0ae0f" target="_blank" class="btn btn-primary px-5 py-3">Get Started</a></p> 
              
            </div>
          </div>
        </div>
  
        
      </div>  
  
      <div class="site-blocks-cover overlay overlay-2" style="background-image: url(images/hero_2.jpg);" data-aos="fade" id="home-section">
  
  
        <div class="container">
          <div class="row align-items-center justify-content-center">
            <div class="col-md-6 mt-lg-5 text-center">
              <h1 class="text-shadow">Giving You The Perfect Place To Stay.</h1>
              <p class="mb-5 text-shadow">We do all the work, whilst you do all the staying ...</p>
              <p><a href="https://ecashmeup.com/rentalapplication?guid=768c586c2b9e069c96f33dae9eb0ae0f" target="_blank" class="btn btn-primary px-5 py-3" >Get Started</a></p>  
              
            </div>
          </div>
        </div>
  
        
      </div>  
    </div>   
    
  </div>      

<!--- About Us will be fitted in this block --> 
    <section class="site-section" id="MessageFromTheCEO-section">
      <div class="container">
        
        <div class="row large-gutters">
          <div class="col-lg-6 mb-5">

              <div class="owl-carousel slide-one-item with-dots">
                  <div><img src="images/CEO Mulunghisi Mahwayi.png" alt="Image" class="img-fluid"></div>
                </div>

          </div>
          <div class="col-lg-6 ml-auto">
            
            <h2 class="section-title mb-3">Message From The CEO</h2>
                <p class="lead">Mulunghisi Mahwayi</p> 
				<ul class="list-unstyled ul-check success">
                  <p>We have set out our tasks and objectives for the years to come; this gives us a chance to reflect on what we have achieved in the past years. It provides us the opportunity to examine where we have come from, where we are now and where we would like to be in the future. We are a company that is determined in creating an integrated organisation that could offer a range of products in order to.: 
</p> 
                      
                  <li>Meet a growing need for accommodation;</li> 
				  <li>Developing and bringing up to standard an existing accommodation industry which suffers from lack of maintenance, security and many other experiences; </li>
				  <li>Establish choices in accommodation that meet the demands of tenants; </li>
				  <li>Implement programs and services that could support social development and ensure that all buildings are enhanced in the efficiency of their use;</li>
				  <li>Develop a single, effective organisational structure across South Africa;</li>
				  <li>Introduce the modern business systems that could support our organisation; and </li>
				  <li>Recruit and develop the staff with the skills and aptitudes to deliver our vision of making our cities feels like home.
</li>
             
            
          </div>
        </div>
      </div>
    </section>
        
        
  <div class="site-section" id="rentals-section">
      <div class="container">
        <div class="row large-gutters">
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-5 ">
            <div class="ftco-media-1">
              <div class="ftco-media-1-inner">
                <a href="https://ecashmeup.com/rentalapplication?guid=768c586c2b9e069c96f33dae9eb0ae0f" target="blank" class="d-inline-block mb-4"><img src="images/Property%20for%20sale%201.jpeg" alt="FImageo" class="img-fluid"></a>
                <div class="ftco-media-details">
                  <h3>For Sale.</h3>
                  <p>Bloemfontein/Upmarket</p>
                  <strong>R1,500,000.00</strong>
                </div>
  
                  
              </div> 
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-5 ">
              <div class="ftco-media-1">
                  <div class="ftco-media-1-inner">
                    <a href="https://ecashmeup.com/rentalapplication?guid=768c586c2b9e069c96f33dae9eb0ae0f" target="blank" class="d-inline-block mb-4"><img src="images/property%20For%20sale%202.jpeg" alt="Image" class="img-fluid"></a>
                    <div class="ftco-media-details">
                      <h3>For Sale.</h3>
                      <p>Bloemfontein/Dan Pienaar Drive</p>
                      <strong>R3,800,000.00</strong>
                    </div>
      
                  </div> 
                </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-5 ">
              <div class="ftco-media-1">
                  <div class="ftco-media-1-inner">
                    <a href="https://ecashmeup.com/rentalapplication?guid=768c586c2b9e069c96f33dae9eb0ae0f" target="blank" class="d-inline-block mb-4"><img src="images/Property%20for%20sale%203.jpeg" alt="Image" class="img-fluid"></a>
                    <div class="ftco-media-details">
                      <h3>For Sale</h3>
                      <p>Bloemfontein/Somerton</p>
                      <strong>R1,150,000.00</strong>
                    </div>
      
                  </div> 
                </div>
          </div>

          <div class="col-md-6 col-lg-4 mb-5 mb-lg-5 ">
              <div class="ftco-media-1">
                <div class="ftco-media-1-inner">
                  <a href="https://ecashmeup.com/rentalapplication?guid=768c586c2b9e069c96f33dae9eb0ae0f" target="blank" class="d-inline-block mb-4"><img src="images/Property%20for%20rental%201.jpeg" alt="Image" class="img-fluid"></a>
                  <div class="ftco-media-details">
                    <h3>Rental</h3>
                    <p>Bloemfontein/Fleurdal</p>
                    <strong>R7,500 p/m</strong>
                  </div>
    
                </div> 
              </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-5 mb-lg-5 ">
                <div class="ftco-media-1">
                    <div class="ftco-media-1-inner">
                      <a href="https://ecashmeup.com/rentalapplication?guid=768c586c2b9e069c96f33dae9eb0ae0f" target="blank" class="d-inline-block mb-4"><img src="images/Property%20for%20rental%202.jpeg" alt="Image" class="img-fluid"></a>
                      <div class="ftco-media-details">
                        <h3>Rental</h3>
                        <p>Bloemfontein/Willows, Soete Invaal</p>
                        <strong>R2,800 p/m</strong>
                      </div>
        
                    </div> 
                  </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-5 mb-lg-5 ">
                <div class="ftco-media-1">
                    <div class="ftco-media-1-inner">
                      <a href="https://ecashmeup.com/rentalapplication?guid=768c586c2b9e069c96f33dae9eb0ae0f" target="blank" class="d-inline-block mb-4"><img src="images/Property%20for%20rental%203.jpeg" alt="Image" class="img-fluid"></a>
                      <div class="ftco-media-details">
                        <h3>Rental</h3>
                        <p>Bloemfontein/ Universitas, Piet Lategaan</p>
                        <strong>Price vary p/room</strong>
                      </div>
        
                    </div> 
                  </div>
            </div>

        </div>
      </div>
    </div>
    
    
   <!-- <section class="site-section" id="ourteam-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md-7 text-left">
            <h2 class="section-title mb-3">Our Team</h2>
            <p class="lead">The Individuals that make dream work.</p>
          </div>
        </div>
        <div class="row">
          

           <div class="col-md-6 col-lg-4 mb-4">
            <div class="team-member">
              <figure>
                <ul class="social">
                  <li><a href="#"><span class="icon-facebook"></span></a></li>
                  <li><a href="#"><span class="icon-twitter"></span></a></li>
                  <li><a href="#"><span class="icon-linkedin"></span></a></li>
                  <li><a href="#"><span class="icon-instagram"></span></a></li>
                </ul>
                <img src="images/Mlunghisi%20CEO.jpg" alt="Image" class="img-fluid">
              </figure>
              <div class="p-3 bg-primary">
                <h3 class="mb-2">MULUNGHISI MAHWAYI</h3>
                <span class="position">CHIEF EXECUTIVE OFFICER</span>
              </div>
            </div>
          </div>
            
             <div class="col-md-6 col-lg-4 mb-4">
            <div class="team-member">
              <figure>
                <ul class="social">
                  <li><a href="#"><span class="icon-facebook"></span></a></li>
                  <li><a href="#"><span class="icon-twitter"></span></a></li>
                  <li><a href="#"><span class="icon-linkedin"></span></a></li>
                  <li><a href="#"><span class="icon-instagram"></span></a></li>
                </ul>
                <img src="images/Margaret.jpg" alt="Image" class="img-fluid">
              </figure>
              <div class="p-3 bg-primary">
                <h3 class="mb-2">MARGARET MADUMO</h3>
                <span class="position">CHIEF FINANCIAL OFFICER</span>
              </div>
            </div>
          </div>

            <div class="col-md-6 col-lg-4 mb-4">
            <div class="team-member">
              <figure>
                <ul class="social">
                  <li><a href="#"><span class="icon-facebook"></span></a></li>
                  <li><a href="#"><span class="icon-twitter"></span></a></li>
                  <li><a href="#"><span class="icon-linkedin"></span></a></li>
                  <li><a href="#"><span class="icon-instagram"></span></a></li>
                </ul>
                <img src="images/Essie.jpg" alt="Image" class="img-fluid">
              </figure>
              <div class="p-3 bg-primary">
                <h3 class="mb-2">ESSIE RORKE</h3>
                <span class="position">HEAD OF SALES AND MARKETING</span>
              </div>
            </div>
          </div>
            
            
             <div class="col-md-6 col-lg-4 mb-4">
            <div class="team-member">
              <figure>
                <ul class="social">
                  <li><a href="#"><span class="icon-facebook"></span></a></li>
                  <li><a href="#"><span class="icon-twitter"></span></a></li>
                  <li><a href="#"><span class="icon-linkedin"></span></a></li>
                  <li><a href="#"><span class="icon-instagram"></span></a></li>
                </ul>
                <img src="images/Ntombi.jpg" alt="Image" class="img-fluid">
              </figure>
              <div class="p-3 bg-primary">
                <h3 class="mb-2">NTOMBIFIKILE SITHOLE</h3>
                <span class="position">MANAGING DIRECTOR</span>
              </div>
            </div>
          </div>
          </div>  
      </div>
    </section> --->
<style>
.bg-primary
{
    background-color: #FFD700 !important;
  } 
.text-primary
    {
         color:#FFD700 !important;
    }
</style>
    <section class="py-5 bg-primary site-section What-We-Do" id="whatwedo-section" >
      <div class="container" >
        <div class="row mb-5 justify-content-center">
          <div class="col-md-7 text-center">
            <h2 class="section-title mb-3 text-black" >What We Do</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 text-center">
            <div class="pr-5 first-step">
              <span class="text-black">01.</span>
              <span class="custom-icon flaticon-house text-black"></span>
              <h3 class="text-black">IMPROVE.</h3>
              <p class="text-black">Improve the Current status of Property Management and get investors involved in the growth of their portfolios towards wealth creation.</p>
            </div>
          </div>

          <div class="col-md-4 text-center">
            <div class="pr-5 second-step">
              <span class="text-black">02.</span>
              <span class="custom-icon flaticon-mobile-phone text-black"></span>
              <h3 class="text-dark">REVENUE.</h3>
              <p class="text-black">Increase Revenue substantially by almost 30% for investors which will encourage more investment opportunities and partnering with us towards their growth.</p>
            </div>
          </div>
            
          <div class="col-md-4 text-center">
            <div class="pr-5 second-step">
              <span class="text-black">03.</span>
              <span class="custom-icon flaticon-location text-black"></span>
              <h3 class="text-dark">SUCCESS.</h3>
              <p class="text-black">Investor Revenue Increases, Maintenance is seamless, and growth is inevitable.</p>
            </div>
          </div>
        </div> 
    </section>  


    

     <section class="site-section bg-light" id="services-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-12 text-center">
            <h2 class="section-title mb-3">Services</h2>
          </div>
        </div>
        <div class="row align-items-stretch">
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up">
            <div class="unit-4 d-flex">
              <div class="unit-4-icon mr-4"><span class="text-primary flaticon-house"></span></div>
              <div>
                <h3>Setting Goals</h3>
                <p>We assist you with setting SMART goals, that will assist you in reaching you Objective</p>
                
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="100">
            <div class="unit-4 d-flex">
              <div class="unit-4-icon mr-4" ><span class="text-primary flaticon-mobile-phone" ></span></div>
              <div>
                <h3>Find Property.</h3>
                <p>We guide you and assisting in finding the best possible investment options, whilst avioding unforeseen potholes. </p>
                
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="200">
            <div class="unit-4 d-flex">
              <div class="unit-4-icon mr-4"><span class="text-primary flaticon-home"></span></div>
              <div>
                <h3>Buy Property and Financing.</h3>
                <p>We take you through the processing of informed buying/purchasing and renovations</p>
               
              </div>
            </div>
          </div>


          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="300">
            <div class="unit-4 d-flex">
              <div class="unit-4-icon mr-4"><span class="text-primary flaticon-flat"></span></div>
              <div>
                <h3>Tenant Placement and Rental Collection</h3>
                <p>We collect Rental payments via Debit Order and ensure we place the right tenant in the right property.</p>
               
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="400">
            <div class="unit-4 d-flex">
              <div class="unit-4-icon mr-4"><span class="text-primary flaticon-location"></span></div>
              <div>
                <h3>Property Management and Maintenance.</h3>
                <p>We allow you the previlage of setting back and watching your investment grow at an affordable cost</p>
                
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="500">
            <div class="unit-4 d-flex">
              <div class="unit-4-icon mr-4"><span class="text-primary flaticon-mobile-phone"></span></div>
              <div>
                <h3>Free Advice and Coaching</h3>
                <p>Property is about patience, persistence and continuous learing. Nobody knows it all. In this industry, you are always learning something new. This is why we offer you free coaching ...</p>
                
              </div>
            </div>
          </div>

        </div>
      </div>
    </section>


    <section class="site-section testimonial-wrap" id="testimonials-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-12 text-center">
            <h2 class="section-title mb-3">Testimonials<h2>
          </div>
        </div>

        <div class="row">
            <div class="col-md-6 mb-4">
                <div class="ftco-testimonial-1">
                  <div class="ftco-testimonial-vcard d-flex align-items-center mb-4">
                    <img src="images/Keys.jfif" alt="Image" class="img-fluid mr-3">
                    <div>
                      <h3>JACQUES</h3>
                      <span>Previous Tenant</span>
                    </div>
                  </div>
                  <div>
                    <p>Thank you Ntombi for your honesty and ensuring our needs are attended too. We appreciate your work and are sure to refer others to you</p>
                  </div>
                </div>
              </div>
              <div class="col-md-6 mb-4">
                  <div class="ftco-testimonial-1">
                      <div class="ftco-testimonial-vcard d-flex align-items-center mb-4">
                        <img src="images/Windows.jfif" alt="Image" class="img-fluid mr-3">
                        <div>
                          <h3>Bongani Zulu</h3>
                          <span>Tenant</span>
                        </div>
                      </div>
                      <div>
                        <p>The service received from this company has been great and we will recommand you to others</p>
                      </div>
                    </div>
              </div> 

              <div class="col-md-6 mb-4">
                  <div class="ftco-testimonial-1">
                    <div class="ftco-testimonial-vcard d-flex align-items-center mb-4">
                      <img src="images/Lights.jfif" alt="Image" class="img-fluid mr-3">
                      <div>
                        <h3>Mpho Khosi</h3>
                        <span>Previous Tenant</span>
                      </div>
                    </div>
                    <div>
                      <p>Thank you Intelli Property Management for being  so considerate and being true to your word</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 mb-4">
                    <div class="ftco-testimonial-1">
                        <div class="ftco-testimonial-vcard d-flex align-items-center mb-4">
                          <img src="images/Clock.jfif" alt="Image" class="img-fluid mr-3">
                          <div>
                            <h3>ABIGAIL BOTHA</h3>
                            <span>Previous Tenant</span>
                          </div>
                        </div>
                        <div>
                          <p>Definitely need a bit of improvement but your customer care is pleasantly marvellous and you stick to the bottom line</p>
                        </div>
                      </div>
                </div> 
        </div>
      </div>
    </section>
    
    <section class="site-section" id="successstories-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-12 text-center">
            <h2 class="section-title mb-3">Success Stories</h2>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
            <div class="h-entry">
              <!--<a href="single.html"><img src="images/Success2.jpeg" alt="Image" class="img-fluid"></a> -->
              <h2 class="font-size-regular"><a href="https://www.buildinganddecor.co.za/irene-court-from-eyesore-to-urban-asset/" target="blank" class="d-inline-block mb-4" style= "color: #FFD700"><img src="images/Success2.jpeg" alt="Image" class="img-fluid">Irene court from eyesore to urban</a></h2>
              <div class="meta mb-4">Posted on <span class="mx-2">&bullet;</span> Aug 17, 2020<span class="mx-2">&bullet;</span> <a href="single.html" style= "color: #FFD700">Article</a></div>
            </div>  
          </div>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
            <div class="h-entry">
              <!-- <a href="single.html"><img src="images/Success1.jpeg" alt="Image" class="img-fluid"></a> -->
              <h2 class="font-size-regular"><a href="https://www.buildinganddecor.co.za/irene-court-from-eyesore-to-urban-asset/" target="blank" class="d-inline-block mb-4" style= "color: #FFD700"><img src="images/Success1.jpeg" alt="Image" class="img-fluid">Irene court from eyesore to urban</a></h2>
              <div class="meta mb-4">Posted on <span class="mx-2">&bullet;</span> Aug 17, 2020<span class="mx-2">&bullet;</span> <a href="single.html" style= "color: #FFD700">Article</a></div>
              
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
            <div class="h-entry">
              <!-- <a href="single.html"><img src="images/Success3.jpeg" alt="Image" class="img-fluid"></a> -->
              <h2 class="font-size-regular"><a href="https://www.buildinganddecor.co.za/irene-court-from-eyesore-to-urban-asset/" target="blank" class="d-inline-block mb-4" style= "color: #FFD700"><img src="images/Success3.jpeg" alt="Image" class="img-fluid">Irene court from eyesore to urban</a></h2>
              <div class="meta mb-4">Posted on <span class="mx-2">&bullet;</span> Aug 17, 2020<span class="mx-2">&bullet;</span> <a href="single.html" style= "color: #FFD700">Article</a></div>
            
			</div> 
          </div>
          
        </div>
      </div>
    </section>


   


    <section class="site-section bg-light bg-image" id="contact-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-12 text-center">
            <h2 class="section-title mb-3">Contact Us</h2>
          </div>
        </div>
        <div class="row">
          <div id="divcontent" class="col-md-7 mb-5">

            

            <form class="p-5 bg-white">
              
              <h2 class="h4 text-black mb-5">Get In Touch</h2> 

              <div class="row form-group">
                
                <div class="col-md-12">
                  <label class="text-black" for="email">Email</label> 
                  <input type="email" id="email" name="email"  class="form-control">
                </div>
              </div>
             <div class="row form-group">
                 
                <div class="col-md-6 mb-3 mb-md-0">
                  <label class="text-black" for="fname">First Name</label>
                  <input type="text" id="fname" class="form-control">
                </div>
                <div class="col-md-6">
                  <label class="text-black" for="lname">Last Name</label>
                  <input type="text" id="lname" class="form-control">
                </div>
              </div>
              <div class="row form-group">
                
                <div class="col-md-12">
                  <label class="text-black" for="subject">Subject</label> 
                  <input type="subject" id="subject" name="subject"  class="form-control">
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-12">
                  <label class="text-black" for="message">Message</label> 
                  <textarea name="message" id="message" name="message"  cols="30" rows="7" class="form-control" placeholder="Write your notes or questions here..."></textarea>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-12">
                 <!-- <input type="submit" value="Send Message" class="btn btn-primary btn-md text-white"> -->
                   
                    <type="button" class="btn btn-primary btn-md text-white" onclick= "validate_save()">Send Message</button>    
                </div>
              </div>

  
            </form>
          </div>
          <div class="col-md-5">
            
            <div class="p-4 mb-3 bg-white">
              <p class="mb-0 font-weight-bold">Address</p>
              <p class="mb-4">Clearwater Office Park, Strubens Valley, Johannesburg, 1735</p>
                

              <p class="mb-0 font-weight-bold">Phone</p>
              <p class="mb-4"><a href="#" style= "color: #FFD700">010-007-5969</a></p>

              <p class="mb-0 font-weight-bold">Whatsapp</p>
              <p class="mb-4"><a href="#" style= "color: #FFD700">051-012-5012</a></p>
                
              <p class="mb-0 font-weight-bold">Email Address</p>
              <p class="mb-0"><a href="#" style= "color: #FFD700">info@intellipropertymanagement.com</a></p>

            </div>
            
          </div>
        </div>
      </div>
    </section>
          
<!--- New form Report a fault ---->

    <section class="site-section bg-light bg-image" id="reportafault-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-12 text-center">
            <h2 class="section-title mb-3">Report a Fault</h2>
          </div>
        </div>
        <div class="row">
          <div id="divcontent" class="col-md-7 mb-5">

            

            <form class="p-5 bg-white">
              
              <h2 class="h4 text-black mb-5">Report your Fault</h2> 

             <div class="row form-group">
                 
                <div class="col-md-6 mb-3 mb-md-0">
                  <label class="text-black" for="tname">Tenant Name</label>
                  <input type="text" id="tname" class="form-control">
                </div>
                <div class="col-md-6">
                  <label class="text-black" for="Resident address">Resident Address</label>
                  <textarea name="Resident address" id="Residentaddress" name="Resident Address"  cols="30" rows="7" class="form-control" placeholder="Write your address here..."></textarea>
                </div>
              </div>
                
                <div class="row form-group">
                
                <div class="col-md-12">
                  <label class="text-black" for="email">Email</label> 
                  <input type="email" id="email" name="email"  class="form-control">
                </div>
                <div class="col-md-6">
                  <label class="text-black" for="contactnum">Contact Number</label>
                  <input type="text" id="contactnum" class="form-control" value maxlength="14">
                </div>
              </div>
              <div class="row form-group">
                
                <div class="col-md-12">
                  <label class="text-black" for="Nature of the fault">Nature of the fault</label> 
                  <input type="subject" id="NoF" name="subject"  class="form-control">
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-12">
                  <label class="text-black" for="message">Message</label> 
                  <textarea name="message" id="message" name="message"  cols="30" rows="7" class="form-control" placeholder="Write your notes or questions here..."></textarea>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-12">
                 <!-- <input type="submit" value="Send Message" class="btn btn-primary btn-md text-white"> -->
                   
                    <type="button" class="btn btn-primary btn-md text-white" onclick= "validate_save_report()">Report Fault</button>    
                </div>
              </div>

  
            </form>
          </div>
         
        <!--    <div class="col-md-5">
            
            <div class="p-4 mb-3 bg-white">
              <p class="mb-0 font-weight-bold">Address</p>
              <p class="mb-4">Clearwater Office Park, Strubens Valley, Johannesburg, 1735</p>
                

              <p class="mb-0 font-weight-bold">Phone</p>
              <p class="mb-4"><a href="#" style= "color: #FFD700">010-007-5969</a></p>

              <p class="mb-0 font-weight-bold">Whatsapp</p>
              <p class="mb-4"><a href="#" style= "color: #FFD700">051-012-5012</a></p>
                
              <p class="mb-0 font-weight-bold">Email Address</p>
              <p class="mb-0"><a href="#" style= "color: #FFD700">info@intellipropertymanagement.com</a></p>

            </div>
            
          </div> --->
        </div>
      </div>
    </section>          
          
          
    
  <footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <div class="row">
              <div class="col-md-5">
                <h2 class="footer-heading mb-4">Decliamer</h2>
                <p>Powered by Vogsphere</p>
                <p>Intelli Property Management, your trusted property management entity. Been operating since 2012</p>
              </div>
              
              
            </div>
          </div>
       <!--   <div class="col-md-4">
            <div class="mb-4">
              <h2 class="footer-heading mb-4">Subscribe </h2>
            <form action="#" method="post" class="footer-subscribe">
              <div class="input-group mb-3">
                <input type="text" class="form-control border-secondary text-white bg-transparent" placeholder="Enter Email" aria-label="Enter Email" aria-describedby="button-addon2">
                <div class="input-group-append">
                  <button class="btn btn-primary text-black" type="button" id="button-addon2">Send</button>
                </div>
              </div>
            </form>  
            </div> -->
            
            <div class="">
              <h2 class="footer-heading mb-4">Follow Us</h2>
                <a href="https://www.facebook.com/Intellipropza/" target="blank" class="pl-0 pr-3"><span class="icon-facebook"></span></a>
                <a href="https://twitter.com/intellipropza?s=11" target="blank" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
                <a href="https://instagram.com/intelliprop_za?igshid=1qtx43mitx9ol" target="blank" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
                <a href="#" class="pl-3 pr-3"><span class="icon-linkedin"></span></a>
            </div>


          </div>
        </div>
        <div class="row pt-5 mt-5 text-center">
          <div class="col-md-12">
            <div class="border-top pt-5">
            <p class="copyright">
            <!-- Link back to Si_Tshaba can't be removed. Template is licensed under CC BY 3.0. -->
            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This Website was developed by Si-Tshaba Consulting <i class="icon-heart text-danger" aria-hidden="true"></i> by <a href="https://SiTshaba.com" target="_blank" >Si_Tshaba</a>
            </p>
            </div>
          </div>
          
        </div>
      </div>
    </footer>

  </div> <!-- .site-wrap -->

  <a href="#top" class="gototop"><span class="icon-angle-double-up"></span></a> 

  
<script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.countdown.min.js"></script>
  <script src="js/bootstrap-datepicker.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.fancybox.min.js"></script>
  <script src="js/jquery.sticky.js"></script>

  
  <script src="js/main.js"></script>
        
        
<script>
        function validate_save()
    {
        
        FirstName = document.getElementById('fname').value;
        LastName = document.getElementById('lname').value;
        email = document.getElementById('email').value;
        subject = document.getElementById('subject').value;
        msg = document.getElementById('message').value;
       alert(FirstName+LastName+email+subject+msg); 
	   
	   jQuery(document).ready(function(){
						jQuery.ajax({  type: "POST",
									   url:"Jquery_contactus.php",
									   data:{msgV:msg,FirstNameV:FirstName,LastNameV:LastName,emailV:email,subjectV:subject},
										success: function(data)
										 {
											// alert(data);
											jQuery("#divcontent").html(data);
										 }
									});
								});

    }
        </script> 
        
<script>
    function validate_save_report()
    {
        
        TenantName = document.getElementById('tname').value;
        ResidentAddress = document.getElementById('Residentaddress').value;
        email = document.getElementById('email').value;
        ContactNumber = document.getElementById('contactnum').value;
        Natureofthefault = document.getElementById('NoF').value;
        msg = document.getElementById('message').value;
        alert(TenantName+ResidentAddress+email+ContactNumber+Natureofthefault+msg); 
	   
	   jQuery(document).ready(function(){
						jQuery.ajax({  type: "POST",
									   url:"Jquery_reportfault.php",
									   data:{msgV:msg,TenantNameV:TenantName,ResidentAddressV:ResidentAddress,emailV:email,ContactNumberV:ContactNumber,NatureofthefaultV:Natureofthefault},
										success: function(data)
										 {
											// alert(data);
											jQuery("#divcontent").html(data);
										 }
									});
								});

    }
        </script> 
    
  </body>
</html>